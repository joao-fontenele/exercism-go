# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=${PWD}/bin
else
GOBIN=$(shell go env GOBIN)
endif

# Setting SHELL to bash allows bash commands to be executed by recipes.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

all: run-docker

# Location to install dependencies to
LOCALBIN ?= $(shell pwd)/bin
$(LOCALBIN):
	mkdir -p $(LOCALBIN)

.PHONY: proto
proto:
	protoc --go_out=. --go_opt=paths=source_relative \
        --go-grpc_out=. --go-grpc_opt=paths=source_relative \
        proto/echo.proto

.PHONY: install-tools
install-tools: $(LOCALBIN)
	cd tools ; PATH=${PATH}:${GOBIN} GOBIN=${GOBIN} ./install.sh

.PHONY: run
run: gomodtidy
	$(LOCALBIN)/reflex -d none -s -r '\.go$$' go run main.go server

.PHONY: build
build:
	builder=$(shell docker buildx create --use)
	docker buildx build --push --platform linux/arm64/v8,linux/amd64 --tag joao-fontenele/demo-app:$(shell git rev-parse HEAD) .
	docker buildx rm $(builder)

.PHONY: run-docker
run-docker: build
	docker run --rm -p "8080:8080" -p "50051:50051" -it joao-fontenele/demo-app:latest

.PHONY: fmt
fmt:
	@if goimports -l -w . | grep . ; then \
		echo "goimports found files that need to be formatted"; \
		exit 1; \
	fi

.PHONY: lint
lint:
	$(LOCALBIN)/golangci-lint run ./...
	$(LOCALBIN)/lint . || echo "soft fail, because linter limitations" # zerolog linter

.PHONY: test
test:
	go test ./...

.PHONY: gomodtidy
gomodtidy:
	go mod tidy

.PHONY: govulncheck
govulncheck:
	$(LOCALBIN)/govulncheck ./...

.PHONY: ci
ci: install-tools fmt lint test
