package lasagna

const DefaultPrepTimePerLayer = 2
const DefaultPortions = 2.0
const GramsOfNoodlePerLayer = 50
const LitersOfSaucePerLayer = 0.2

func PreparationTime(layers []string, avgPrepTime int) int {
	if avgPrepTime == 0 {
		return len(layers) * DefaultPrepTimePerLayer
	}
	return len(layers) * avgPrepTime
}

func Quantities(layers []string) (int, float64) {
	sauceLayers, noodleLayers := 0, 0
	for _, l := range layers {
		if l == "sauce" {
			sauceLayers++
		} else if l == "noodles" {
			noodleLayers++
		}
	}
	return GramsOfNoodlePerLayer * noodleLayers, LitersOfSaucePerLayer * float64(sauceLayers)
}

func AddSecretIngredient(friendsList, myList []string) {
	myList[len(myList)-1] = friendsList[len(friendsList)-1]
}

func ScaleRecipe(quantities []float64, portions int) []float64 {
	scaledQtys := make([]float64, len(quantities))
	scale := float64(portions) / DefaultPortions
	for i, qty := range quantities {
		scaledQtys[i] = scale * qty
	}
	return scaledQtys
}
