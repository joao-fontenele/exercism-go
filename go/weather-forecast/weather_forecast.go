// Package weather is responsible for forecasting weather conditions in various Goblinocous cities.
package weather

// CurrentCondition holds the state for the weather condition.
var CurrentCondition string

// CurrentLocation holds the state of the current city.
var CurrentLocation string

// Forecast returns the current weather condition of a city.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
