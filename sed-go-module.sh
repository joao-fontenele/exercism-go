#!/bin/bash

set -euxo pipefail

old_module_path="gitlab.com/joao-fontenele/exercism-go"

new_module_path=$(git config --get remote.origin.url)
new_module_path=$(echo "$new_module_path" | cut -d ":" -f 1 | sed 's/^git@//')'/'$(echo "$new_module_path" | cut -d ":" -f 2 | sed 's/\.git$//')

if [[ "$OSTYPE" == "darwin"* ]]; then
  grep -rl $old_module_path . | xargs sed -i '' "s|$old_module_path|$new_module_path|g"
else
  grep -rl $old_module_path . | xargs sed -i "s|$old_module_path|$new_module_path|g"
fi
