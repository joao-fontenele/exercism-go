#!/bin/bash

set -euxo pipefail

if [[ -z "${GOBIN+x}" ]]; then
  echo "please set GOBIN first"
  exit 1
fi


# leave out versions to use the ones specified in go.mod

# add new tools below
go install github.com/cespare/reflex
go install golang.org/x/vuln/cmd/govulncheck
go install github.com/rs/zerolog/cmd/lint
go install github.com/spf13/cobra-cli
go install golang.org/x/tools/cmd/goimports
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
go install google.golang.org/protobuf/cmd/protoc-gen-go


# protoc is not a go binary to be installed
PROTOC_VERSION=22.2
PROTOC_ZIP_FILE=protoc-${PROTOC_VERSION}-$(uname -s | tr "[:upper:]" "[:lower:]")-$(uname -m).zip
PROTOC_DOWNLOAD_URL=https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOC_VERSION}/${PROTOC_ZIP_FILE}
if which protoc >/dev/null; then
  echo "protoc already installed"
else
  echo "installing protoc ${PROTOC_VERSION}"
  TMP_DIR=$(mktemp -d)
  cd "${TMP_DIR}" ; curl -LO "${PROTOC_DOWNLOAD_URL}"
  unzip "${TMP_DIR}/${PROTOC_ZIP_FILE}" -d "${TMP_DIR}"
  cp "${TMP_DIR}/bin/protoc" "${GOBIN}"
  rm -fr "${TMP_DIR}"
fi


# official docs recommended installing like this instead of using traditional go install command
GOLANGCI_LINT_VERSION=v1.55.2
if which golangci-lint >/dev/null; then
  echo "golangci-lint is already installed"
else
  curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b "${GOBIN}" ${GOLANGCI_LINT_VERSION}
fi

EXERCISM_CLI_VERSION=3.2.0
if which exercism >/dev/null; then
  echo "exercism is already installed"
else
  URL="https://github.com/exercism/cli/releases/download/v${EXERCISM_CLI_VERSION}/exercism-${EXERCISM_CLI_VERSION}-linux-x86_64.tar.gz"
  TMP_DIR=$(mktemp -d)
  cd "${TMP_DIR}" ; curl -sLO "$URL"
  tar -xzf "$TMP_DIR/exercism-${EXERCISM_CLI_VERSION}-linux-x86_64.tar.gz"
  mv "$TMP_DIR/exercism" "${GOBIN}"
  rm -fr "$TMP_DIR"
  echo "Exercism CLI has been installed successfully!"
fi

